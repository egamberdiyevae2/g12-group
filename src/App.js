import Footer from "./components/Footer";
import Nav from "./components/Nav";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/homePage";
import Fees from "./pages/fees";
import Contact from "./pages/contact";
import WhyUs from "./pages/whyUs";
import HowWork from "./pages/howItWorks";
import About from "./pages/about";
import Prepaid from "./pages/prepaidCard";

function App() {
  return (
    <div className="App">
      <Nav />
      <main>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/why" element={<WhyUs />} />
          <Route path="/how" element={<HowWork />} />
          <Route path="/about" element={<About />} />
          <Route path="/prepaid" element={<Prepaid />} />

          <Route path="/fees" element={<Fees />} />

          <Route path="/contact" element={<Contact />} />
        </Routes>
      </main>
      <Footer />
    </div>
  );
}

export default App;
