import React from "react";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
import mobile from "../images/mobile 2.png";
import prepaidCard from "../images/prepaidCard.svg";
import getPayment from "../images/getPayment.svg";
import transferMoney from "../images/transferMoney.svg";
import girl from "../images/girl-good 1.png";

const HomePage = () => {
  return (
    <div className="HomePage">
      <header className="header">
        <div className="container">
          <div className="topDiv">
            <div className="topDiv__texts">
              <h1 className="topDiv__texts__h1">
                Safe & Easy payments solution worldwide!
              </h1>
              <p className="topDiv__texts__p">
                If you, too, are among the handful of people who are afraid (and
                rightly so) to hand over their credit information online, we're
                here for you!
              </p>
              <p className="topDiv__texts__p">
                TheKalibris allows you to get a virtual credit card number in
                less than 60 seconds and pay with it online! What are you
                waiting for?
              </p>
              <button className="topDiv__texts__btn">Sign up</button>
            </div>
            <img src={mobile} alt="mobile phone" />
          </div>
        </div>
        <div className="bottomDiv">
          <div className="bottomDiv__card">
            <img src={prepaidCard} alt="icon" />
            <p className="bottomDiv__card__PGT">Prepaid card</p>
            <p className="bottomDiv__card__texts">
              Just in few moments after you sign up you will be able to receive
              virtual or physical prepaid card to make your payments safe &
              easy!
            </p>
          </div>
          <div className="bottomDiv__card">
            <img src={getPayment} alt="icon" />
            <p className="bottomDiv__card__PGT">Get your payments safely</p>
            <p className="bottomDiv__card__texts">
              Just in few moments after you sign up you will be able to receive
              virtual or physical prepaid card to make your payments safe &
              easy!
            </p>
          </div>
          <div className="bottomDiv__card">
            <img src={transferMoney} alt="icon" />
            <p className="bottomDiv__card__PGT">Transfer money fast</p>
            <p className="bottomDiv__card__texts">
              Just in few moments after you sign up you will be able to receive
              virtual or physical prepaid card to make your payments safe &
              easy!
            </p>
          </div>
        </div>
      </header>
      <section className="oneSystem"></section>
    </div>
  );
};

export default HomePage;
