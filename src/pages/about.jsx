import React, { Component } from "react";

export default class about extends Component {
  render() {
    return (
      <div className="container">
        <div className="qovun">
          <div className="secText">
            <h1>About</h1>
          </div>
          <div className="secText2">
            <h4>
              TheKalibris was founded in 2015 as a company <br /> specializing
              in payment solutions and receiving <br />
              money online in a safe, fast and efficient method.
            </h4>
          </div>
        </div>
        <div className="svgDiv">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="974"
            height="2"
            viewBox="0 0 974 2"
            fill="none"
          >
            <path d="M0 1H974" stroke="#2D404E" />
          </svg>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="58"
            height="56"
            viewBox="0 0 58 56"
            fill="none"
          >
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M30.0732 10.8834C36.1118 6.47233 41.773 3.08107 49.229 0C49.1605 0.975602 48.7788 3.10692 46.7964 4.29432C43.1279 5.86529 39.135 7.81983 35.7874 10.0479C34.198 11.1052 32.4673 12.366 30.6989 13.7218C30.5513 12.765 30.3426 11.8182 30.0741 10.8867L30.0732 10.8834Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M35.6685 10.9688C43.1972 5.85485 53.697 2.27014 57.2114 1.14111C55.6333 3.36093 53.7224 5.33771 51.5426 7.00556C49.5397 7.70849 47.2475 8.524 46.3591 8.87922C44.8851 9.46958 37.9538 12.5848 36.0519 13.4412C35.9943 12.6093 35.8664 11.7835 35.6693 10.9722L35.6685 10.9688Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M36.0782 14.1889C37.4477 13.5702 45.0989 10.1281 46.6293 9.51936C47.8841 9.01906 52.0671 7.55482 54.1094 6.84521C52.6543 8.6013 49.2673 12.3569 45.7991 13.7078C44.5152 13.9454 43.197 14.2014 42.3257 14.4049C40.5599 14.8168 37.244 15.8766 35.8788 16.3227C36.017 15.6218 36.084 14.9093 36.0791 14.1956L36.0782 14.1889Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M35.683 17.1126C36.5843 16.8174 40.543 15.5283 42.4894 15.0755C44.0301 14.7161 47.0533 14.1774 48.7378 13.8848C47.0627 15.309 43.3599 18.1682 40.3111 18.4576L35.0137 18.8078C35.2898 18.2637 35.5139 17.6959 35.683 17.1118V17.1126Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M34.6147 19.5295L43.9092 18.9133C42.5055 19.924 39.526 21.7067 36.0175 21.655L33.4232 21.1739C33.8622 20.6557 34.2605 20.106 34.6147 19.5295Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M32.892 21.781L31.9411 22.8C31.7006 23.0543 31.336 23.4154 30.8798 23.8523L32.316 24.4518C35.6413 25.475 38.4145 23.8565 39.5571 23.0176L32.892 21.781Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M30.3274 24.3768L36.0527 26.7666C34.9058 27.2786 32.4681 28.1142 30.4096 27.1269L28.4744 26.0996C29.1591 25.4742 29.7839 24.8888 30.3274 24.376V24.3768Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M27.9302 26.5994L34.0483 29.8514C32.9964 30.2374 30.8069 30.8153 28.5627 30.0373L25.9033 28.453C26.5949 27.826 27.2797 27.2022 27.931 26.6069L27.9302 26.5994Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M25.3615 28.9346L31.2939 32.4776C30.4765 32.8111 28.8503 33.2689 27.1478 32.5076L23.7378 30.4005L25.3641 28.9362L25.3615 28.9346Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M23.1951 30.887L26.6625 33.03L27.1777 35.4706L21.3531 32.5363L23.1951 30.887Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M20.7942 33.0356L27.3592 36.3435L27.7563 38.2539C25.4316 37.646 21.2752 36.2835 18.9668 34.6625L20.7934 33.0365L20.7942 33.0356Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M38.5649 39.5625L28.9341 33.6113C30.0806 33.6318 31.2118 33.3531 32.2106 32.8042V32.2121L30.3275 31.0898C31.0667 31.1366 31.809 31.0943 32.5375 30.9639C34.8579 33.3762 36.8539 36.348 38.5615 39.5625H38.5649Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M29.6071 34.8491L30.9766 38.9591C30.2328 38.8274 29.4076 38.6598 28.5209 38.4463C28.045 36.1532 27.6906 34.468 27.4938 33.5466L29.6019 34.8491H29.6071Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M31.7722 39.09L30.5508 35.4319L33.8033 37.439L34.542 39.4052C33.9908 39.3694 33.0313 39.2843 31.7756 39.09H31.7722Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M39.2926 50.0356L34.7861 38.0498L39.1864 40.769L39.29 40.9766C41.5676 45.5728 43.2889 50.5542 44.5557 55.0503C43.3257 54.4441 40.704 52.8823 39.2909 50.0356H39.2926Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M34.8037 40.114L40.0249 54.0167C38.7581 53.433 36.133 51.8345 34.8311 48.2631L32.0134 39.8271C33.3829 40.0273 34.3681 40.094 34.7995 40.1148L34.8037 40.114Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M31.228 39.703L35.6421 52.907C34.4318 52.7578 31.2486 51.7788 30.0109 45.6767C29.5119 43.2193 29.0634 41.0355 28.6825 39.1943C29.6103 39.4095 30.4671 39.5754 31.2315 39.703H31.228Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M31.4171 22.3322C29.5648 24.3042 19.993 32.8136 18.395 34.232C17.5704 33.5751 16.8479 32.8052 16.25 31.9464C17.9088 30.5772 26.0076 23.8815 27.8119 22.2138C29.3466 20.7962 31.2399 19.0451 30.7982 14.5265C32.2533 13.4042 33.6853 12.3435 35.0394 11.4021C36.301 17.1606 33.5552 20.0666 31.6191 22.1137L31.4145 22.3322H31.4171Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M27.2079 21.8159C25.5585 23.3435 17.571 29.9467 15.8642 31.3551C15.2974 30.4073 14.842 29.4003 14.5067 28.3532C15.6913 27.2692 23.0558 20.5617 30.1198 15.0483C30.3518 18.9091 28.6844 20.4492 27.3218 21.7074L27.2079 21.8125V21.8159Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M30.0439 14.2237C27.6157 16.1074 25.1395 18.1428 22.8772 20.0606C22.4493 19.3819 22.3132 18.0219 22.1326 16.9921C24.7072 14.9483 27.1038 13.073 29.4602 11.3328C29.7243 12.2815 29.919 13.2473 30.0431 14.2229L30.0439 14.2237Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M16.1735 11.054C14.7509 10.6671 12.6436 10.9606 11.1046 11.2808L10.3103 10.9164C7.64751 9.73732 3.96016 8.43735 0.0639496 8.0813L-0.000244141 8.77089C3.76586 9.11277 7.33767 10.3619 9.93628 11.5067L11.0678 12.0262C11.9024 12.4507 12.5229 12.8025 12.8653 13.0035C14.0122 11.5218 16.4439 12.2655 16.5133 14.1175C19.147 14.8822 19.6194 19.5817 20.1638 22.3876C20.8546 21.7881 21.5821 21.1627 22.3379 20.519C20.8469 18.3843 22.575 12.7925 16.1752 11.0515L16.1735 11.054Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M16.4103 14.8159C18.616 15.6005 18.9969 20.4027 19.5464 22.9242C18.7555 23.6138 18.022 24.2584 17.3629 24.8421C16.9204 20.6978 15.7076 17.0873 15.3147 15.9933C15.571 15.881 15.8007 15.7183 15.9894 15.5154C16.1781 15.3125 16.3217 15.0737 16.4111 14.8142L16.4103 14.8159Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M14.6204 16.1642C14.991 17.1832 16.3126 21.0539 16.7046 25.43C15.5919 26.4214 14.7479 27.1861 14.2772 27.6163C13.6404 25.4233 13.2783 22.8634 12.8863 20.4777C12.3727 17.3675 11.3764 14.8818 9.81262 12.1826C11.153 12.8222 12.1159 13.3717 12.543 13.6243C12.4491 13.9301 12.4325 14.2535 12.4947 14.5669C12.5569 14.8802 12.696 15.1743 12.9002 15.4238C13.1044 15.6734 13.3676 15.8711 13.6673 16.0001C13.967 16.129 14.2943 16.1853 14.6213 16.1642H14.6204Z"
              fill="#F4545A"
            />
            <path
              fill-rule="evenodd"
              clip-rule="evenodd"
              d="M15.9897 8.53594L11.4344 7.47529L12.4119 3.48784L13.8576 4.96208L14.0818 5.19139L14.3369 4.9946L17.4465 2.59229L19.107 6.10528L19.2423 6.39212L19.5487 6.28706L21.5225 5.60831L20.545 9.59576L15.9897 8.53594Z"
              fill="#F4545A"
            />
          </svg>
        </div>

        <div className="tarvuz">
          <div className="tarText">
            <p>
              TheKalibris saves you the most expensive thing for you, the time.
              <br />
              You do not have to wait in the long exhausting there is no need to
              wait a <br /> few days for the approval to send you the card. In
              our company you <br />
              can receive it within a few seconds after making the order. The
              code <br />
              (CVV) on the back of the card will be sent to you by SMS and the
              cost <br /> of the card is minimal. <br /> <br /> <br />
              Sounds great? It's not all! In addition, you can see the details
              of the <br /> transactions immediately in your account and stay
              updated on the <br />
              status of your purchases and the amount of money remaining in your
              <br />
              account.
              <br />
              Sounds great? It’s not all! In addition, you can see the details
              of the <br /> transactions immediately in your account and stay
              updated on the <br />
              status of your purchases and the amount of money remaining in your
              <br />
              account.
            </p>
          </div>
          <div className="tarText2">
            <p>
              We used the best technological innovations, recruited the best
              minds <br /> and established a friendly and easy operation system
              so that the <br /> average user could experience the system’s
              efficiency at his first entry. <br /> The system is up-to-date and
              advanced and draws real-time data from <br /> our encrypted
              databases. Purchase data is updated instantly and there <br /> is
              no need for unnecessary waiting time. This means that virtual{" "}
              <br />
              services can be verified in the first few minutes after receiving
              the card <br /> number.
            </p>
          </div>
        </div>
      </div>
    );
  }
}
