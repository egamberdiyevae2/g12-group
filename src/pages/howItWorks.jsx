import React, { Component } from "react";

export default class howIt extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="text-img">
            <div className="how-it">
              <h1 className="WhyUse__title">How it Works</h1>
              <p className="WhyUse__text">
                Our system allows you to send and receive <br /> money, as well
                as issue bank cards with a <br /> positive balance.{" "}
              </p>
            </div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="58"
              height="56"
              viewBox="0 0 58 56"
              fill="none"
            >
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M30.0732 10.8834C36.1118 6.47233 41.7729 3.08107 49.2289 0C49.1605 0.975602 48.7787 3.10692 46.7964 4.29432C43.1278 5.86529 39.1349 7.81983 35.7874 10.0479C34.1979 11.1052 32.4672 12.366 30.6989 13.7218C30.5512 12.765 30.3426 11.8182 30.074 10.8867L30.0732 10.8834Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M35.6685 10.9688C43.1972 5.85485 53.697 2.27014 57.2114 1.14111C55.6333 3.36093 53.7224 5.33771 51.5426 7.00556C49.5397 7.70849 47.2475 8.524 46.3591 8.87922C44.8851 9.46958 37.9538 12.5848 36.0519 13.4412C35.9943 12.6093 35.8664 11.7835 35.6693 10.9722L35.6685 10.9688Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M36.0785 14.1889C37.448 13.5702 45.0992 10.1281 46.6296 9.51936C47.8844 9.01906 52.0674 7.55482 54.1096 6.84521C52.6545 8.6013 49.2676 12.3569 45.7993 13.7078C44.5154 13.9454 43.1973 14.2014 42.3259 14.4049C40.5601 14.8168 37.2442 15.8766 35.879 16.3227C36.0172 15.6218 36.0843 14.9093 36.0793 14.1956L36.0785 14.1889Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M35.6829 17.1126C36.5842 16.8174 40.5429 15.5283 42.4893 15.0755C44.03 14.7161 47.0532 14.1774 48.7377 13.8848C47.0626 15.309 43.3598 18.1682 40.311 18.4576L35.0135 18.8078C35.2897 18.2637 35.5137 17.6959 35.6829 17.1118V17.1126Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M34.6148 19.5295L43.9093 18.9133C42.5056 19.924 39.5261 21.7067 36.0177 21.655L33.4233 21.1739C33.8623 20.6557 34.2606 20.106 34.6148 19.5295Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M32.8919 21.781L31.941 22.8C31.7005 23.0543 31.3358 23.4154 30.8796 23.8523L32.3159 24.4518C35.6412 25.475 38.4143 23.8565 39.557 23.0176L32.8919 21.781Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M30.3276 24.3768L36.0529 26.7666C34.9059 27.2786 32.4682 28.1142 30.4097 27.1269L28.4745 26.0996C29.1592 25.4742 29.7841 24.8888 30.3276 24.376V24.3768Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M27.93 26.5994L34.0482 29.8514C32.9963 30.2374 30.8068 30.8153 28.5626 30.0373L25.9032 28.453C26.5948 27.826 27.2795 27.2022 27.9309 26.6069L27.93 26.5994Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M25.3614 28.9346L31.2938 32.4776C30.4764 32.8111 28.8501 33.2689 27.1477 32.5076L23.7377 30.4005L25.3639 28.9362L25.3614 28.9346Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M23.1949 30.887L26.6622 33.03L27.1775 35.4706L21.3529 32.5363L23.1949 30.887Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M20.7943 33.0356L27.3593 36.3435L27.7565 38.2539C25.4318 37.646 21.2754 36.2835 18.9669 34.6625L20.7935 33.0365L20.7943 33.0356Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M38.5649 39.5625L28.9341 33.6113C30.0806 33.6318 31.2118 33.3531 32.2106 32.8042V32.2121L30.3275 31.0898C31.0667 31.1366 31.809 31.0943 32.5375 30.9639C34.8579 33.3762 36.8539 36.348 38.5615 39.5625H38.5649Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M29.6068 34.8491L30.9763 38.9591C30.2325 38.8274 29.4074 38.6598 28.5206 38.4463C28.0448 36.1532 27.6904 34.468 27.4935 33.5466L29.6017 34.8491H29.6068Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M31.7722 39.09L30.5508 35.4319L33.8033 37.439L34.542 39.4052C33.9908 39.3694 33.0313 39.2843 31.7756 39.09H31.7722Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M39.2926 50.0356L34.7861 38.0498L39.1864 40.769L39.29 40.9766C41.5676 45.5728 43.2889 50.5542 44.5557 55.0503C43.3257 54.4441 40.704 52.8823 39.2909 50.0356H39.2926Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M34.8037 40.114L40.0249 54.0167C38.7581 53.433 36.133 51.8345 34.8311 48.2631L32.0134 39.8271C33.3829 40.0273 34.3681 40.094 34.7995 40.1148L34.8037 40.114Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M31.2279 39.703L35.642 52.907C34.4317 52.7578 31.2485 51.7788 30.0108 45.6767C29.5118 43.2193 29.0633 41.0355 28.6824 39.1943C29.6102 39.4095 30.467 39.5754 31.2313 39.703H31.2279Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M31.4171 22.3322C29.5648 24.3042 19.993 32.8136 18.395 34.232C17.5704 33.5751 16.8479 32.8052 16.25 31.9464C17.9088 30.5772 26.0076 23.8815 27.8119 22.2138C29.3466 20.7962 31.2399 19.0451 30.7982 14.5265C32.2533 13.4042 33.6853 12.3435 35.0394 11.4021C36.301 17.1606 33.5552 20.0666 31.6191 22.1137L31.4145 22.3322H31.4171Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M27.2078 21.8159C25.5584 23.3435 17.5708 29.9467 15.8641 31.3551C15.2973 30.4073 14.8419 29.4003 14.5066 28.3532C15.6912 27.2692 23.0557 20.5617 30.1197 15.0483C30.3517 18.9091 28.6843 20.4492 27.3217 21.7074L27.2078 21.8125V21.8159Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M30.0441 14.2237C27.6158 16.1074 25.1396 18.1428 22.8773 20.0606C22.4494 19.3819 22.3133 18.0219 22.1327 16.9921C24.7073 14.9483 27.1039 13.073 29.4603 11.3328C29.7244 12.2815 29.9192 13.2473 30.0432 14.2229L30.0441 14.2237Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M16.1737 11.054C14.7511 10.6671 12.6438 10.9606 11.1049 11.2808L10.3106 10.9164C7.64776 9.73732 3.9604 8.43735 0.0641937 8.0813L0 8.77089C3.7661 9.11277 7.33791 10.3619 9.93652 11.5067L11.0681 12.0262C11.9026 12.4507 12.5232 12.8025 12.8655 13.0035C14.0125 11.5218 16.4442 12.2655 16.5135 14.1175C19.1472 14.8822 19.6197 19.5817 20.1641 22.3876C20.8548 21.7881 21.5823 21.1627 22.3381 20.519C20.8471 18.3843 22.5752 12.7925 16.1754 11.0515L16.1737 11.054Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M16.4102 14.8159C18.6159 15.6005 18.9968 20.4027 19.5463 22.9242C18.7554 23.6138 18.0219 24.2584 17.3628 24.8421C16.9203 20.6978 15.7074 17.0873 15.3146 15.9933C15.5709 15.881 15.8006 15.7183 15.9893 15.5154C16.1779 15.3125 16.3215 15.0737 16.411 14.8142L16.4102 14.8159Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M14.6204 16.1642C14.991 17.1832 16.3126 21.0539 16.7046 25.43C15.5919 26.4214 14.7479 27.1861 14.2772 27.6163C13.6404 25.4233 13.2783 22.8634 12.8863 20.4777C12.3727 17.3675 11.3764 14.8818 9.81262 12.1826C11.153 12.8222 12.1159 13.3717 12.543 13.6243C12.4491 13.9301 12.4325 14.2535 12.4947 14.5669C12.5569 14.8802 12.696 15.1743 12.9002 15.4238C13.1044 15.6734 13.3676 15.8711 13.6673 16.0001C13.967 16.129 14.2943 16.1853 14.6213 16.1642H14.6204Z"
                fill="#F4545A"
              />
              <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M15.9896 8.53594L11.4343 7.47529L12.4118 3.48784L13.8575 4.96208L14.0817 5.19139L14.3368 4.9946L17.4464 2.59229L19.1069 6.10528L19.2421 6.39212L19.5486 6.28706L21.5223 5.60831L20.5449 9.59576L15.9896 8.53594Z"
                fill="#F4545A"
              />
            </svg>
          </div>
          <div className="securePayment">
            <div>
              <h3>Registration to the system</h3> <br />
              <p>
                First you need to fill out a minimum registration form with
                personal <br /> details and choose a password – in minutes you
                will receive a <br />
                verification code, which you will need to insert in order to
                enter the <br /> system and after a few minutes the system will
                be ready to use.
              </p>
              <h3>Start using the system</h3> <br />
              <p>
                At this stage you can start receiving payments from other
                account <br /> owners, all you have to do is give your friend
                the phone number you <br /> registered with the system, and
                after selecting the transfer amount,
                <br /> and confirming the process, the money will arrive in your
                account.
                <br />
                Another way to receive money is through a unique and dedicated{" "}
                <br /> clearing terminal that the system will provide for you -
                which you can <br /> embed in your online store or business site
                - you will receive the <br /> money directly to your account and
                you can withdraw it at any time to <br /> your bank account
              </p>
            </div>
            <div>
              <h3> Getting a prepaid card with a click</h3> <br />
              <p>
                Our system provides a management panel for the various
                operations <br /> and constant monitoring. Here you can also
                order a prepaid card, and <br /> recharge it in a variety of
                ways, by making a deposit in your account, by
                <br /> telephone or simply transferring money to your account.
                <br />
                Enter the credit card receipt page, go through the steps and in
                a few
                <br /> seconds receive a prepaid credit card number, Available
                only for our verified users!
              </p>
            </div>
          </div>
        </div>
      </>
    );
  }
}
